--
-- Enhancements to tt_content table
--
create table tt_content (
	input0 varchar(255) default '' not null,
	input1 varchar(255) default '' not null,
	input2 varchar(255) default '' not null,
	input3 varchar(255) default '' not null,
	input4 varchar(255) default '' not null,

	`text` text,
	text0 text,
	text1 text,
	text2 text,
	text3 text,
	text4 text,

	link varchar(1024) default '' not null,
	link0 varchar(1024) default '' not null,
	link1 varchar(1024) default '' not null,
	link2 varchar(1024) default '' not null,
	link3 varchar(1024) default '' not null,
	link4 varchar(1024) default '' not null,

	image0 int unsigned default 0 not null,
	image1 int unsigned default 0 not null,
	image2 int unsigned default 0 not null,
	image3 int unsigned default 0 not null,
	image4 int unsigned default 0 not null,

	assets0 int unsigned default 0 not null,
	assets1 int unsigned default 0 not null,
	assets2 int unsigned default 0 not null,
	assets3 int unsigned default 0 not null,
	assets4 int unsigned default 0 not null,

	`check` tinyint unsigned default 0 not null,
	check0 tinyint unsigned default 0 not null,
	check1 tinyint unsigned default 0 not null,
	check2 tinyint unsigned default 0 not null,
	check3 tinyint unsigned default 0 not null,
	check4 tinyint unsigned default 0 not null,

	element int unsigned default null,
	element0 int unsigned default null,
	element1 int unsigned default null,
	element2 int unsigned default null,

	elements text,
	elements0 text,
	elements1 text,
	elements2 text,

	nested_content int unsigned default 0 not null,
	nested_content0 int unsigned default 0 not null,
	nested_content1 int unsigned default 0 not null,
	nested_content2 int unsigned default 0 not null,

	nested_content_parent int unsigned default 0 not null,
	nested_content_parent0 int unsigned default 0 not null,
	nested_content_parent1 int unsigned default 0 not null,
	nested_content_parent2 int unsigned default 0 not null
);
