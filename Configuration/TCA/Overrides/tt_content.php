<?php
defined('TYPO3_MODE') or die();

(function (string $table) {

    // ------------------------- additional columns -------------------------
    foreach(['0', '1', '2', '3', '4'] as $i) {
        $GLOBALS['TCA'][$table]['columns']['input' . $i] = [
            'exclude' => true,
            'label' => 'Input',
            'config' => [
                'type' => 'input',
                'eval' => 'trim',
                'max' => 255,
            ],
        ];
    }

    foreach(['', '0', '1', '2', '3', '4'] as $i) {
        $GLOBALS['TCA'][$table]['columns']['text' . $i] = [
            'exclude' => true,
            'label' => 'Text',
            'config' => [
                'type' => 'text',
            ],
        ];
    }

    foreach(['', '0', '1', '2', '3', '4'] as $i) {
        $GLOBALS['TCA'][$table]['columns']['link' . $i] = [
            'exclude' => true,
            'label' => 'Link',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputLink',
                'eval' => 'trim',
                'max' => 1024,
                'size' => 50,
            ],
        ];
    }

    for ($i = 0; $i < 5; $i++) {
        $GLOBALS['TCA'][$table]['columns']['image' . (string)$i] = [
            'label' => 'Bild',
            'exclude' => true,
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'image' . (string)$i,
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:media.addFileReference',
                    ],
                    'maxitems' => 1,
                    'overrideChildTca' => [
                        'types' => [
                            '0' => [
                                'showitem' => '
                            --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                                'showitem' => '
                            --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                'showitem' => '
                            --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                                'showitem' => '
                            --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.audioOverlayPalette;audioOverlayPalette,
                            --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                                'showitem' => '
                            --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.videoOverlayPalette;videoOverlayPalette,
                            --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                                'showitem' => '
                            --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                            ],
                        ],
                    ],
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),
        ];
    }

    for ($i = 0; $i < 5; $i++) {
        $GLOBALS['TCA'][$table]['columns']['assets' . (string)$i] = [
            'label' => 'Dateien',
            'exclude' => true,
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'assets' . (string)$i,
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:media.addFileReference',
                    ],
                    'maxitems' => PHP_INT_MAX,
                ]
            ),
        ];
    }

    foreach(['', '0', '1', '2', '3', '4'] as $i) {
        $GLOBALS['TCA'][$table]['columns']['check' . $i] = [
            'exclude' => true,
            'label' => 'Check',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ],
        ];
    }

    foreach(['', '0', '1', '2'] as $i) {
        $GLOBALS['TCA'][$table]['columns']['element' . $i] = [
            'exclude' => true,
            'label' => 'Ausgewähltes Element',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'pages', // TCA requires this to exist, overwrite as necessary
                'maxitems' => 1,
                'size' => 1,
            ],
        ];
    }

    foreach(['', '0', '1', '2'] as $i) {
        $GLOBALS['TCA'][$table]['columns']['elements' . $i] = [
            'exclude' => true,
            'label' => 'Ausgewählte Elemente',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'pages', // TCA requires this to exist, overwrite as necessary
                'maxitems' => PHP_INT_MAX,
                'size' => 10,
            ],
        ];
    }

    foreach([0 => '', 1 => '0', 2 => '1', 3 => '2'] as $k => $v) {
        $GLOBALS['TCA'][$table]['columns']['nested_content' . $v] = [
            'label' => 'Nested content',
            'exclude' => true,
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tt_content',
                'overrideChildTca' => [
                    'columns' => [
                        'colPos' => [
                            'config' => [
                                'default' => 999 + $k,
                            ],
                        ],
                    ],
                ],
                'foreign_sortby' => 'sorting',
                'appearance' => [
                    'collapseAll' => true,
                    'showSynchronizationLink' => true,
                    'showPossibleLocalizationRecords' => true,
                    'showAllLocalizationLink' => true,
                    'useSortable' => true,
                    'enabledControls' => [
                        'dragdrop' => true,
                    ],
                ],
                'foreign_field' => 'nested_content_parent' . $v,
            ],
        ];
    }
})('tt_content');
