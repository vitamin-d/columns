Columns
=======

Provides additional generic tt_content columns for own content elements.

## Usage

When creating your content elements, use these predefined columns in addition to default tt_content columns before
defining your own:

* Input fields (*one line, max 255 chars*)
  * `input0`, `input1`, `input2`, `input3`, `input4`
  * Note: These fields are *trimmed* by default
* Text fields (*multiple lines, no size restriction*)
  * `text`, `text0`, `text1`, `text2`, `text3`, `text4`
  * Note: `bodytext` exists already in TYPO3 CMS's *frontend* extension
* Link fields (*TypoLink*)
  * `link`, `link0`, `link1`, `link2`, `link3`, `link4`
* Image (one)
  * `image0`, `image1`, `image2`, `image3`, `image4`
  * Note: `image` exists already in TYPO3 CMS's *frontend* extension
* Arbitrary files (multiple)
  * `assets0`, `assets1`, `assets2`, `assets3`, `assets4`
  * Note: `assets` exists already in TYPO3 CMS's *frontend* extension
* Checkboxes
  * `check`, `check0`, `check1`, `check2`, `check3`, `check4`
* Nested Content
  * `nested_content`, `nested_content0`, `nested_content1`, `nested_content2`


In your `Configuration/TCA/Overrides/tt_content.php`:

```php
$GLOBALS['TCA']['tt_content]['types']['yourext_yourcontentelement'] = [
    'showitem' => '
            input0;Extra text,
            check;Be awesome,
            image0;Background image,
    ',
    'columnsOverrides' => [
        'input0' => [
            'config' => [
                'default' => 'quux',
                'eval' => 'required, trim',
            ],
        ],
        'image0' => [
            'config' => [
                'minitems' => 1, // required
            ],
        ],
    ],
];
```
